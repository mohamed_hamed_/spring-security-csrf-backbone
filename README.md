This project was the base of a blog post I wrote <http://jpgmr.wordpress.com/2014/01/03/spring-security-csrf-protection-in-a-backbone-single-page-app/>
which attempts to achieve CSRF protection using Spring Security in a Backbone single page app.  Feel free to refer to that post for explanations on code and my thought process.

# Building this project #

   1. Before building the client side code, you will need npm, bower and grunt installed.
   2. Go into the webapp directory "cd src/main/webapp".
   3. Run "npm install" to install the necessary npm modules.
   4. Run "bower install" to install the necessary bower compoments needed by this app.
   5. Run "grunt" to build/compile the client side code.
   6. Back in the root of the project, run "mvn clean install" to build the war.

# Test the app with Jetty #

   1. If you want to just test the app now run "mvn jetty:run" and browse to <http://localhost:8080/test-csrf>
      where the default username and password are "admin" and "admin".
