package com.patrickgrimard.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;

import javax.inject.Inject;

/**
 * Main security configuration for app.
 *
 * @author Patrick Grimard
 * @since 11/22/2013 1:41 PM
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Setup in memory authentication.
     *
     * @param auth
     * @throws Exception
     */
    @Inject
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .inMemoryAuthentication()
                    .withUser("admin").password("admin").roles("USER", "ADMIN");
    }

    /**
     * Configure HttpSecurity, namely setting the CsrfTokenRepository to our own implementation
     * of HttpHeaderCsrfTokenRepository.
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .addFilterAfter(new CsrfResponseHeaderFilter(), CsrfFilter.class)
                .authorizeRequests()
                    .antMatchers("/scripts/**", "/styles/**", "/font/**", "/fonts/**").permitAll()
                    .antMatchers("/**").authenticated()
                    .and()
                .formLogin()
                    .loginPage("/login").permitAll();
    }
}
