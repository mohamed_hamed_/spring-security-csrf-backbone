package com.patrickgrimard.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Main @Bean config.
 *
 * @author Patrick Grimard
 * @since 2013-06-12 9:51 AM
 */
@Configuration
@ComponentScan
public class AppConfig {

    private static final Logger logger = LoggerFactory.getLogger(AppConfig.class);
}
