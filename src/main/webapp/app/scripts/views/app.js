/*global webapp, Backbone, JST, _*/

webapp.Views = webapp.Views || {};

(function () {
  'use strict';

  webapp.Views.AppView = Backbone.View.extend({

    template: JST['app/scripts/templates/app.ejs'],

    initialize: function() {
      this.render();
    },

    render: function() {
      this.$el.html(_.template(this.template(this.model)));
      return this;
    }

  });

})();
